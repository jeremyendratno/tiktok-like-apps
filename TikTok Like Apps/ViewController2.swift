//
//  ViewController2.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/18/22.
//

import UIKit
import AVFoundation

class ViewController2: UIViewController, AVAssetDownloadDelegate, AVAssetResourceLoaderDelegate {
    let downloadIdentifier = "discover"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAssetDownload()
    }
    
    func setupAssetDownload() {
        // Create new background session configuration.
        let configuration = URLSessionConfiguration.background(withIdentifier: downloadIdentifier)
     
        // Create a new AVAssetDownloadURLSession with background configuration, delegate, and queue
        let downloadSession = AVAssetDownloadURLSession(configuration: configuration, assetDownloadDelegate: self, delegateQueue: OperationQueue.main)
        
        // HLS Asset URL
        let url = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4")!
        let url2 = URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2996%2Fb2e24972-5f40-4787-b11f-42470d0562af.m3u8?alt=media")!
        let asset = AVURLAsset(url: url)
     
        // Create new AVAssetDownloadTask for the desired asset
        let downloadTask = downloadSession.makeAssetDownloadTask(asset: asset, assetTitle: downloadIdentifier, assetArtworkData: nil, options: nil)
        // Start task and begin download
        downloadTask?.resume()
    }
    
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        return true
    }
    
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
        print(location)
        UserDefaults.standard.set(location.relativePath, forKey: "assetPath")
    }
}
