//
//  VideoCollectionViewCell.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/14/22.
//

// MARK: - Import library
import Foundation
import UIKit
import FloatingHearts

// MARK: - Main class variables
class VideoCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlet variables
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var agentImageView: UIImageView!
    @IBOutlet weak var agentNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var seeMoreLabel: UILabel!
    @IBOutlet weak var likeCounterLabel: UILabel!
    @IBOutlet weak var viewCounterLabel: UILabel!
    @IBOutlet weak var viewImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    
    // MARK: - Main class variables
    var explore: Explore?
    
    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        agentImageSetup()
    }
    
    // MARK: - Agent image setup
    func agentImageSetup() {
        agentImageView.layer.cornerRadius = 75 / 2
        agentImageView.layer.borderColor = UIColor.white.cgColor
        agentImageView.layer.borderWidth = 2
    }
    
    // MARK: - Data setup
    func dataSetup() {
        agentImageView.sd_setImage(with: URL(string: explore?.user?.photo?.small ?? ""))
        agentNameLabel.text = explore?.user?.name ?? ""
        contentLabel.text = explore?.caption ?? ""
        titleLabel.text = explore?.title ?? ""
        likeCounterLabel.text = "\(explore?.like ?? 0)"
        viewCounterLabel.text = "\(explore?.view ?? 0)"
        
        // Hide or show see more depending content label's max lines
        if contentLabel.calculateMaxLines() >= 3 {
            let tap = UITapGestureRecognizer(target: self, action: #selector(seeMore))
            seeMoreLabel.addGestureRecognizer(tap)
            seeMoreLabel.isHidden = false
        } else {
            seeMoreLabel.isHidden = true
        }
        
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(like))
        likeImageView.addGestureRecognizer(likeTap)
    }
    
    // MARK: - See more
    @objc func seeMore() {
        if seeMoreLabel.text == "See More" {
            seeMoreLabel.text = "Hide"
            contentLabel.numberOfLines = 0
            contentLabel.text = explore?.caption ?? ""
        } else if seeMoreLabel.text == "Hide" {
            seeMoreLabel.text = "See More"
            contentLabel.numberOfLines = 3
            contentLabel.text = explore?.caption ?? ""
        }
    }
    
    @objc func like() {
//        let heart = HeartView(frame: CGRect(x: 0, y: 0, width: 36, height: 0.1))
//        view.addSubview(heart)
//        let fountainX = (view.frame.width - 30)
//        let fountainY = self.stackTool.frame.minY
//        heart.center = CGPoint(x: fountainX, y: fountainY)
//        heart.animateInView(view)
    }
}
