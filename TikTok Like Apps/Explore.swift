//
//  Explore.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/28/22.
//

import Foundation

struct Explore: Decodable {
    var className: String?
    var lastEdited: String?
    var created: String?
    var title: String?
    var videoLink: String?
    var caption: String?
    var urlSegment: String?
    var thumbLink: String?
    var abnormalFile: Bool?
    var deviceDescription: String?
    var authorClass: String?
    var relatedPropertyID: String?
    var id: Int?
    var isLike: Bool?
    var isView: Bool?
    var isShare: Bool?
    var link: String?
    var like: Int?
    var view: Int?
    var share: Int?
    var user: Agent?
    
    enum CodingKeys: String, CodingKey {
        case className = "ClassName"
        case lastEdited = "LastEdited"
        case created = "Created"
        case title = "Title"
        case videoLink = "VideoLink"
        case caption = "Caption"
        case urlSegment = "URLSegment"
        case thumbLink = "ThumbLink"
        case abnormalFile = "AbnormalFile"
        case deviceDescription = "DeviceDescription"
        case authorClass = "AuthorClass"
        case relatedPropertyID = "RelatedPropertyID"
        case id = "ID"
        case isLike = "IsLike"
        case isView = "IsView"
        case isShare = "IsShare"
        case link = "Link"
        case like = "Like"
        case view = "View"
        case share = "Share"
        case user = "User"
    }
    
    init(decode: Decoder) throws {
        let value = try decode.container(keyedBy: CodingKeys.self)
        className = try value.decodeIfPresent(String.self, forKey: .className)
        lastEdited = try value.decodeIfPresent(String.self, forKey: .lastEdited)
        created = try value.decodeIfPresent(String.self, forKey: .created)
        title = try value.decodeIfPresent(String.self, forKey: .title)
        videoLink = try value.decodeIfPresent(String.self, forKey: .videoLink)
        caption = try value.decodeIfPresent(String.self, forKey: .caption)
        urlSegment = try value.decodeIfPresent(String.self, forKey: .urlSegment)
        thumbLink = try value.decodeIfPresent(String.self, forKey: .thumbLink)
        abnormalFile = try value.decodeIfPresent(Bool.self, forKey: .abnormalFile)
        deviceDescription = try value.decodeIfPresent(String.self, forKey: .deviceDescription)
        authorClass = try value.decodeIfPresent(String.self, forKey: .authorClass)
        relatedPropertyID = try value.decodeIfPresent(String.self, forKey: .relatedPropertyID)
        id = try value.decodeIfPresent(Int.self, forKey: .id)
        isLike = try value.decodeIfPresent(Bool.self, forKey: .isLike)
        isView = try value.decodeIfPresent(Bool.self, forKey: .isView)
        isShare = try value.decodeIfPresent(Bool.self, forKey: .isShare)
        link = try value.decodeIfPresent(String.self, forKey: .link)
        like = try value.decodeIfPresent(Int.self, forKey: .like)
        view = try value.decodeIfPresent(Int.self, forKey: .view)
        share = try value.decodeIfPresent(Int.self, forKey: .share)
        user = try value.decodeIfPresent(Agent.self, forKey: .user)
    }
}
