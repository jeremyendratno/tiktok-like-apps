//
//  ViewController4.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 4/4/22.
//

// MARK: - Import Library
import UIKit
import Player
import AVFoundation
import SDWebImage

// MARK: - Main class
class ViewController4: UIViewController {
    
    // MARK: - IBOutlet variables
    @IBOutlet weak var playerCollectionView: UICollectionView!
    @IBOutlet weak var loadingImageView: UIImageView!
    
    // MARK: - Main class variables
    let viewModel = ExploreViewModel()
    var players: [Player?] = []

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
    }
    
    // MARK: - Setups
    func viewSetup() {
        viewModelSetup()
        playerCollectionViewSetup()
        loadingSetup()
    }
     
    func viewModelSetup() {
        viewModel.delegate = self
        viewModel.requestExplores()
    }
     
    func playerCollectionViewSetup() {
        playerCollectionView.delegate = self
        playerCollectionView.dataSource = self
        playerCollectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
     
    func loadingSetup() {
        loadingImageView.image = UIImage.gif(name: "interwind")
    }
     
    // Adding the first 3 player on setup
    func playerFirstSetup() {
        for i in 0 ... 2 {
            players.append(addPlayer(url: viewModel.explores[i].videoLink ?? ""))
        }
        
        playerCollectionView.reloadData()
    }
    
    // MARK: - Add Player
    func addPlayer(url: String) -> Player {
        let player = Player()
        player.playerDelegate = self
        player.playbackDelegate = self
        player.playbackLoops = true
        player.view.frame = self.view.bounds
        player.fillMode = .resizeAspectFill
        let url = URL(string: url)!
        let asset = AVURLAsset(url: url)
        player.asset = asset
        return player
    }
    
    // MARK: - Scroll view delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        // Adding this logic so that no cell / player is going to be skipped
        playerCollectionView.isScrollEnabled = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Adding this logic so that no cell / player is going to be skipped
        playerCollectionView.isScrollEnabled = true
        
        // Getting scroll direction
        var scrollDirection = ""
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview!)
        
        if translation.y > 0 {
           scrollDirection = "Down"
        } else {
            scrollDirection = "Up"
        }
        
        // Getting viewed index
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400))?.row else { return }
        
        scrollLogic(viewedIndex: viewedIndex, direction: scrollDirection)
    }
    
    // MARK: - Scroll logic
    func scrollLogic(viewedIndex: Int, direction: String) {
        // Playing only player that user is viewing and pause all other players after user scrolled
        for i in 0 ... players.count - 1 {
            if i == viewedIndex {
                players[i]?.playFromCurrentTime()
            } else {
                players[i]?.pause()
            }
        }
        
        // Pagination is activated if user viewed last 5 player
        if viewedIndex > viewModel.explores.count - 5 {
            viewModel.requestExplores()
        }
        
        // Logic if user scrolled up
        // Adding player to players array on viewed index + 2 and destroying player on viewed index - 3
        if direction == "Up" {
            if viewedIndex - 3 >= 0 {
                players[viewedIndex - 3] = nil
            }
            
            if players.count > viewedIndex + 2 {
                players[viewedIndex + 2] = addPlayer(url: viewModel.explores[viewedIndex + 2].videoLink ?? "")
            } else {
                if viewModel.explores.count > viewedIndex + 2 {
                    players.append(addPlayer(url: viewModel.explores[viewedIndex + 2].videoLink ?? ""))
                } 
            }
            
            debugInformation(viewedIndex: viewedIndex)
            playerCollectionView.reloadData()
            
        // Logic if user scrolled down
        // Adding player to players array on viewed index - 2 and destroying player on viewed index + 3
        } else if direction == "Down" {
            if players.count > viewedIndex + 3 {
                players[viewedIndex + 3] = nil
            }
            
            if viewedIndex - 2 >= 0 {
                players[viewedIndex - 2] = addPlayer(url: viewModel.explores[viewedIndex - 2].videoLink ?? "")
            }
            
            debugInformation(viewedIndex: viewedIndex)
            playerCollectionView.reloadData()
        }
        
        // Loading logic after user scrolled
        if players[viewedIndex]?.bufferingState.description == "Ready" && loadingImageView.isHidden == false {
            loadingImageView.isHidden = true
        } else if players[viewedIndex]?.bufferingState.description != "Ready" && loadingImageView.isHidden == true {
            loadingImageView.isHidden = false
        }
    }
    
    // MARK: - Debug Information
    // This is just debug information for debugging, can command function after debug
    func debugInformation(viewedIndex: Int) {
        print("==================")
        for i in 0 ... players.count - 1 {
            let player = players[i]
                    
            if i == viewedIndex {
                print("Index \(i): \(player?.bufferingState.description ?? "Error") <==")
            } else {
                print("Index \(i): \(player?.bufferingState.description ?? "Error")")
            }
        }
        print("\n")
    }
}

// MARK: - View model handler
extension ViewController4: ExploreDelegate {
    func onDoneExplore() {
        playerFirstSetup()
    }
}

// MARK: - Collection view handler
extension ViewController4: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return players.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Attaching player to the back view of the cell
        let cell = playerCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VideoCollectionViewCell 
        cell.backView.backgroundColor = .black
        cell.backView.subviews.forEach { $0.removeFromSuperview() }
        cell.backView.addSubview((players[indexPath.row]?.view)!)
        cell.backView.bringSubviewToFront((players[indexPath.row]?.view)!)
        
        // Giving cell explore variable and initiate the data setup
        cell.explore = viewModel.explores[indexPath.row]
        cell.dataSetup()
        return cell
    }
}

// MARK: - Player handler
extension ViewController4: PlayerDelegate, PlayerPlaybackDelegate {
    func playerReady(_ player: Player) {
        // Playing only player that user is viewing and pause all other players after player is ready playing
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400)) else { return }
        if player == players[viewedIndex.row] {
            player.playFromCurrentTime()
        } else {
            player.pause()
        }
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        
    }
    
    func playerCurrentTimeDidChange(_ player: Player) {
        // Constantly checking to hide or show loading
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400)) else { return }
        if player == players[viewedIndex.row] {
            if player.bufferingState.description == "Ready" && loadingImageView.isHidden == false {
                loadingImageView.isHidden = true
            } else if player.bufferingState.description != "Ready" && loadingImageView.isHidden == true {
                loadingImageView.isHidden = false
            }
        }
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
        
    }
}
