//
//  ExploreViewModel.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/28/22.
//

import Foundation

protocol ExploreDelegate {
    func onDoneExplore()
}

class ExploreViewModel {
    var delegate: ExploreDelegate?
    var explores: [Explore] = []
    var isLast: Bool = false
    var fetching: Bool = false
    
    func requestExplores() {
        if fetching == false && isLast == false {
            fetching = true
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = "www.brighton.co.id"
            urlComponents.path = "/explore-api/search"
            urlComponents.queryItems = [
                URLQueryItem(name: "ClientID", value: "f9dabd9ada9e1f333c2e4f17380f7620"),
                URLQueryItem(name: "AccessToken", value: "67bffc46e0b836230c5c54034d18c626"),
                URLQueryItem(name: "Start", value: "\(explores.count)"),
                URLQueryItem(name: "Count", value: "10")
            ]
            
            guard let url = urlComponents.url else { return }
            
            let urlConfig = URLSessionConfiguration.default
            urlConfig.timeoutIntervalForRequest = 30
            urlConfig.timeoutIntervalForResource = 30
            urlConfig.waitsForConnectivity = true
            
            let urlSession = URLSession(configuration: urlConfig).dataTask(with: url) { [weak self] data, respone, error in
                if let error = error {
                    print("Cannot get Attends, Error: \(error.localizedDescription)")
                } else {
                    guard let data = data else { return }
                    let decode = try? JSONDecoder().decode(ExploreResponse.self, from: data)
                    self?.explores.append(contentsOf: decode?.data ?? [])
                    if decode?.data?.count ?? 0 < 10 { self?.isLast = true }
                    
                    DispatchQueue.main.async {
                        self?.fetching = false
                        self?.delegate?.onDoneExplore()
                    }
                }
            }
            
            urlSession.resume()
        }
    }
}
