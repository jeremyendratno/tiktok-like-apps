//
//  ExploreResponse.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/28/22.
//

import Foundation

struct ExploreResponse: Decodable { 
    var data: [Explore]?
    
    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
    
    init(decode: Decoder) throws {
        let value = try decode.container(keyedBy: CodingKeys.self)
        data = try value.decodeIfPresent([Explore].self, forKey: .data)
    }
}
