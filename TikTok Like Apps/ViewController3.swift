//
//  ViewController3.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/24/22.
//

import Foundation
import UIKit
import AVFoundation
import CachingPlayerItem
import Player
import SwiftGifOrigin

class ViewController3: UIViewController {
    @IBOutlet weak var playerCollectionView: UICollectionView!
    @IBOutlet weak var loadingImageView: UIImageView!
    
    let viewModel = ExploreViewModel()
    var players: [Player] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.requestExplores()
        playerCollectionView.delegate = self
        playerCollectionView.dataSource = self
        playerCollectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        loadingImageView.image = UIImage.gif(name: "interwind")
    }
    
    func addingPlayer() {
        let count = viewModel.explores.count - players.count
        for _ in 1 ... count {
            let player = Player()
            player.playerDelegate = self
            player.playbackDelegate = self
            player.playbackLoops = true
            player.view.frame = self.view.bounds
            players.append(player)
        }
    }
    
    func addAsset(index: Int) {
        if viewModel.explores.count > index && index >= 0 {
            if players[index].asset == nil {
//                let identifier = "\(viewModel.explores[index].created ?? "")"
                
//                if let assetPath = UserDefaults.standard.string(forKey: identifier) {
//                    let baseURL = URL(fileURLWithPath: NSHomeDirectory())
//                    let assetURL = baseURL.appendingPathComponent(assetPath)
//                    let asset = AVURLAsset(url: assetURL)
//                    players[index].asset = asset
//                } else {
//                    let url = URL(string: viewModel.explores[index].videoLink ?? "")!
//                    let configuration = URLSessionConfiguration.background(withIdentifier: identifier)
//                    let downloadSession = AVAssetDownloadURLSession(configuration: configuration, assetDownloadDelegate: self, delegateQueue: .main)
//                    let asset = AVURLAsset(url: url)
//                    let downloadTask = downloadSession.makeAssetDownloadTask(asset: asset, assetTitle: identifier, assetArtworkData: nil, options: nil)
//                    downloadTask?.resume()
//                    players[index].asset = downloadTask?.urlAsset
//                }
                
                players[index].fillMode = .resizeAspectFill
                let url = URL(string: viewModel.explores[index].videoLink ?? "")!
                let asset = AVURLAsset(url: url)
                players[index].asset = asset
            }
        }
    }
    
    
    
    func checkBuffer(viewedIndex: Int) {
        print("==================")
        for i in 0 ... players.count - 1 {
            if i == viewedIndex {
                print("Player \(i + 1): \(players[i].bufferingState) <==")
            } else {
                print("Player \(i + 1): \(players[i].bufferingState)")
            }
        }
        print("\n")
    }
    
    func changingStatus(viewedIndex: Int) {
        for i in 0 ... players.count - 1 {
            if i == viewedIndex {
                players[i].playFromCurrentTime()
            } else {
                players[i].pause()
            }
        }
        
        addAsset(index: viewedIndex - 2)
        addAsset(index: viewedIndex - 1)
        addAsset(index: viewedIndex)
        addAsset(index: viewedIndex + 1)
        addAsset(index: viewedIndex + 2)
        
        if viewedIndex > viewModel.explores.count - 5 {
            viewModel.requestExplores()
        }
        
        if players[viewedIndex].bufferingState.description == "Ready" {
            loadingImageView.isHidden = true
        } else {
            loadingImageView.isHidden = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollLogic()
    }
    
    func scrollLogic() {
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400)) else { return }
        changingStatus(viewedIndex: viewedIndex.row)
        checkBuffer(viewedIndex: viewedIndex.row)
    }
}

extension ViewController3: ExploreDelegate {
    func onDoneExplore() {
        addingPlayer()
        playerCollectionView.reloadData()
        scrollLogic()
    }
}

extension ViewController3: AVAssetDownloadDelegate {
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
        print("Finish Download: \(location) \(session.configuration.identifier!)")
        if let identifier = session.configuration.identifier {
            UserDefaults.standard.set(location.relativePath, forKey: identifier)
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        print("Error: \(error?.localizedDescription ?? "Cannot Describe Error")")
    }
}

extension ViewController3: PlayerPlaybackDelegate, PlayerDelegate {
    func playerCurrentTimeDidChange(_ player: Player) {
        
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
        
    }
    
    func playerReady(_ player: Player) {
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400)) else { return }
        if player == players[viewedIndex.row] {
            player.playFromCurrentTime()
        } else {
            player.pause()
        }
        
        if player.bufferingState.description == "Ready" {
            loadingImageView.isHidden = false
        } else {
            loadingImageView.isHidden = true
        }
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        guard let viewedIndex = playerCollectionView.indexPathForItem(at: CGPoint(x: playerCollectionView.contentOffset.x, y: playerCollectionView.contentOffset.y + 400)) else { return }
        if player == players[viewedIndex.row] {
            if player.bufferingState.description == "Ready" {
                loadingImageView.isHidden = false
            } else {
                loadingImageView.isHidden = true
            }
        }
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        
    }
}

extension ViewController3: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.explores.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = playerCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VideoCollectionViewCell
        cell.backView.backgroundColor = .black
        cell.backView.subviews.forEach { $0.removeFromSuperview() }
        cell.backView.addSubview(players[indexPath.row].view)
        cell.backView.bringSubviewToFront(players[indexPath.row].view)
        return cell
    }
}
