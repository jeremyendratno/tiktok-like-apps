//
//  ViewController.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 3/14/22.
//

import UIKit
import AVFoundation
import Player

class ViewController: UIViewController, AVAssetDownloadDelegate {
    @IBOutlet weak var videoCollectionView: UICollectionView!
    
    let urls = [
        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2998%2F657038eb-eea8-490c-8a97-935fa988d9cf.m3u8?alt=media",
        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2996%2Fb2e24972-5f40-4787-b11f-42470d0562af.m3u8?alt=media",
        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F3000%2FMP4_20220315_000538.m3u8?alt=media",
        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2990%2FMP4_20220313_203251.m3u8?alt=media",
        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2988%2F950b2b34-7ea6-49c3-a6c7-1d9c6195a85f.m3u8?alt=media",
    ]
    
    let urls2 = [
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4"
    ]
    
    let urls3 = [
        "https://firebasestorage.googleapis.com/v0/b/guessyourwaifu.appspot.com/o/IMG_0148.MOV?alt=media&token=7fb699c1-3b0d-4827-8b21-b4f1948ca246",
        "https://firebasestorage.googleapis.com/v0/b/guessyourwaifu.appspot.com/o/IMG_0148.MOV?alt=media&token=7fb699c1-3b0d-4827-8b21-b4f1948ca246",
        "https://firebasestorage.googleapis.com/v0/b/guessyourwaifu.appspot.com/o/IMG_0148.MOV?alt=media&token=7fb699c1-3b0d-4827-8b21-b4f1948ca246",
        "https://firebasestorage.googleapis.com/v0/b/guessyourwaifu.appspot.com/o/IMG_0148.MOV?alt=media&token=7fb699c1-3b0d-4827-8b21-b4f1948ca246",
        "https://firebasestorage.googleapis.com/v0/b/guessyourwaifu.appspot.com/o/IMG_0148.MOV?alt=media&token=7fb699c1-3b0d-4827-8b21-b4f1948ca246"
    ]
    
    var assets: [AVAsset] = [
        AVAsset(url: URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2998%2F657038eb-eea8-490c-8a97-935fa988d9cf.m3u8?alt=media")!),
        AVAsset(url: URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2996%2Fb2e24972-5f40-4787-b11f-42470d0562af.m3u8?alt=media")!),
        AVAsset(url: URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F3000%2FMP4_20220315_000538.m3u8?alt=media")!),
        AVAsset(url: URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2990%2FMP4_20220313_203251.m3u8?alt=media")!),
        AVAsset(url: URL(string: "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2988%2F950b2b34-7ea6-49c3-a6c7-1d9c6195a85f.m3u8?alt=media")!),
    ]
    var players: [Player] = []
    var nowDownloading: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearCache()
        collectionPrePlay()
        videoCollectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "video")
        videoCollectionView.dataSource = self
        videoCollectionView.delegate = self
        downloadCache(url: urls[nowDownloading])
    }
    
    func playerSetup(player: Player) {
        player.playerDelegate = self
        player.playbackDelegate = self
        player.playbackLoops = true
        player.view.frame = self.view.bounds
    }
    
    func clearCache() {
        let cacheURL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let fileManager = FileManager.default
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory( at: cacheURL, includingPropertiesForKeys: nil, options: [])
            for file in directoryContents {
                do {
                    try fileManager.removeItem(at: file)
                }
                catch let error as NSError {
                    debugPrint("Ooops! Something went wrong: \(error)")
                }

            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func playerPrePlay(player: Player, videoView: UIView, url: String, asset: AVAsset) {
        videoView.subviews.forEach { $0.removeFromSuperview() }
        videoView.addSubview(player.view)
        videoView.bringSubviewToFront(player.view)
         
//        CacheManager.shared.getFileWith(stringUrl: url) { result in
//            switch result {
//            case .success(let url):
//                print(url)
//                player.url = url
//            case .failure(let error):
//                print(error)
//            }
//        }
    }
    
    func checkBuffer() {
        print("==================")
        for i in 0 ... players.count - 1 {
            print("Player \(i + 1): \(players[i].bufferingState)")
        }
        print("\n")
    }
    
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
        print(location)
        print(nowDownloading)
        players[nowDownloading].url = location
        if nowDownloading < urls.count - 1 {
            nowDownloading += 1
            downloadCache(url: urls[nowDownloading])
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = videoCollectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath) as! VideoCollectionViewCell
        cell.backView.backgroundColor = .black
        playerPrePlay(player: players[indexPath.row], videoView: cell.backView, url: urls[indexPath.row], asset: assets[indexPath.row])
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        changingStatus()
        checkBuffer()
    }
    
    func collectionPrePlay() {
        for _ in 0 ... urls.count - 1 {
            let player = Player()
            playerSetup(player: player)
            players.append(player)
        }
    }
    
    func downloadCache(url: String) {
        let indentifier = url
        let url = URL(string: url)!
        let configuration = URLSessionConfiguration.background(withIdentifier: indentifier)
        let downloadSession = AVAssetDownloadURLSession(configuration: configuration, assetDownloadDelegate: self, delegateQueue: OperationQueue.main)
        let asset = AVURLAsset(url: url)
        let downloadTask = downloadSession.makeAssetDownloadTask(asset: asset, assetTitle: indentifier, assetArtworkData: nil, options: nil)
        downloadTask?.resume()
    }
    
    func changingStatus() {
        guard let viewedIndex = videoCollectionView.indexPathForItem(at: CGPoint(x: videoCollectionView.contentOffset.x, y: videoCollectionView.contentOffset.y + 400)) else { return }
        for i in 0 ... players.count - 1 {
            if i == viewedIndex.row {
                players[i].playFromCurrentTime()
            } else {
                players[i].pause()
            }
        }
    }
}

extension ViewController: PlayerPlaybackDelegate, PlayerDelegate {
    func playerCurrentTimeDidChange(_ player: Player) {
        
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
        
    }
    
    func playerReady(_ player: Player) {
        guard let viewedIndex = videoCollectionView.indexPathForItem(at: CGPoint(x: videoCollectionView.contentOffset.x, y: videoCollectionView.contentOffset.y + 400)) else { return }
        if player == players[viewedIndex.row] {
            player.playFromCurrentTime()
        } else {
            player.pause()
        }
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        
    }
}

