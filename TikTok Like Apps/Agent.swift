//
//  Agent.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 4/4/22.
//

import Foundation

struct Agent: Decodable {
    var name: String?
    var photo: Photo?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case photo = "Photo"
    }
    
    init(decode: Decoder) throws {
        let value = try decode.container(keyedBy: CodingKeys.self)
        name = try value.decodeIfPresent(String.self, forKey: .name)
        photo = try value.decodeIfPresent(Photo.self, forKey: .photo) 
    }
}
