////
////  ViewController.swift
////  TikTok Like Apps
////
////  Created by Jeremy Endratno on 3/14/22.
////
//
//import UIKit
//import AVFoundation
//import Player
//
//class ViewController: UIViewController {
//    @IBOutlet weak var videoCollectionView: UICollectionView!
//
//    let urls = [
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2998%2F657038eb-eea8-490c-8a97-935fa988d9cf.m3u8?alt=media",
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2996%2Fb2e24972-5f40-4787-b11f-42470d0562af.m3u8?alt=media",
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F3000%2FMP4_20220315_000538.m3u8?alt=media",
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2990%2FMP4_20220313_203251.m3u8?alt=media",
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2988%2F950b2b34-7ea6-49c3-a6c7-1d9c6195a85f.m3u8?alt=media",
//        "https://firebasestorage.googleapis.com/v0/b/brighton-debc9/o/vid%2F2987%2F309e93b9-a6a2-4b8b-a2cb-77a383de7862.m3u8?alt=media"
//    ]
//    
//    var players: [Player] = []
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        collectionPrePlay()
//        videoCollectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "video")
//        videoCollectionView.dataSource = self
//        videoCollectionView.delegate = self
//    }
//
//    func playerSetup(player: Player) {
//        player.playerDelegate = self
//        player.playbackDelegate = self
//        player.playbackLoops = true
//        player.view.frame = self.view.bounds
//    }
//
//    func playerPrePlay(player: Player, videoView: UIView, url: String) {
//        videoView.subviews.forEach { $0.removeFromSuperview() }
//        videoView.addSubview(player.view)
//        videoView.bringSubviewToFront(player.view)
//        player.url = URL(string: url)
//    }
//
//    func checkBuffer() {
//        print("==================")
//        for i in 0 ... players.count - 1 {
//            print("Player \(i + 1): \(players[i].bufferingState)")
//        }
//        print("\n")
//    }
//}
//
//extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return urls.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = videoCollectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath) as! VideoCollectionViewCell
//        cell.backView.backgroundColor = .black
//        playerPrePlay(player: players[indexPath.row], videoView: cell.backView, url: urls[indexPath.row])
//        return cell
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        changingStatus()
//        checkBuffer()
//    }
//
//    func collectionPrePlay() {
//        for _ in urls {
//            let player = Player()
//            playerSetup(player: player)
//            players.append(player)
//        }
//    }
//
//    func changingStatus() {
//        guard let viewedIndex = videoCollectionView.indexPathForItem(at: CGPoint(x: videoCollectionView.contentOffset.x, y: videoCollectionView.contentOffset.y + 400)) else { return }
//        for i in 0 ... players.count - 1 {
//            if i == viewedIndex.row {
//                players[i].playFromCurrentTime()
//            } else {
//                players[i].pause()
//            }
//        }
//    }
//}
//
//extension ViewController: PlayerPlaybackDelegate, PlayerDelegate {
//    func playerCurrentTimeDidChange(_ player: Player) {
//
//    }
//
//    func playerPlaybackWillStartFromBeginning(_ player: Player) {
//
//    }
//
//    func playerPlaybackDidEnd(_ player: Player) {
//
//    }
//
//    func playerPlaybackWillLoop(_ player: Player) {
//
//    }
//
//    func playerPlaybackDidLoop(_ player: Player) {
//
//    }
//
//    func playerReady(_ player: Player) {
//        guard let viewedIndex = videoCollectionView.indexPathForItem(at: CGPoint(x: videoCollectionView.contentOffset.x, y: videoCollectionView.contentOffset.y + 400)) else { return }
//        if player == players[viewedIndex.row] {
//            player.playFromCurrentTime()
//        } else {
//            player.pause()
//        }
//    }
//
//    func playerPlaybackStateDidChange(_ player: Player) {
//
//    }
//
//    func playerBufferingStateDidChange(_ player: Player) {
//
//    }
//
//    func playerBufferTimeDidChange(_ bufferTime: Double) {
//
//    }
//
//    func player(_ player: Player, didFailWithError error: Error?) {
//
//    }
//}
//
