//
//  Photo.swift
//  TikTok Like Apps
//
//  Created by Jeremy Endratno on 4/4/22.
//

import Foundation

struct Photo: Decodable {
    var id: Int?
    var small: String?
    var medium: String?
    var original: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case small = "Small"
        case medium = "Medium"
        case original = "Original"
    }
    
    init(decode: Decoder) throws {
        let value = try decode.container(keyedBy: CodingKeys.self)
        id = try value.decodeIfPresent(Int.self, forKey: .id)
        small = try value.decodeIfPresent(String.self, forKey: .small)
        medium = try value.decodeIfPresent(String.self, forKey: .medium)
        original = try value.decodeIfPresent(String.self, forKey: .original)
    }
}
